ADD_FILES =  ../varileginterface/IDatenPacket.cpp ../varileginterface/Intercom_datenpacket.cpp  ../varileginterface/Packet_Reciever.cpp ../varileginterface/Sensor_Datenpacket.cpp ../varileginterface/Output_Datenpacket.cpp ../varileginterface/Commands_Datenpacket.cpp
ADD_LIBRARIES = -lzmq

all:  BA



BA: BA_Framework.cpp
	g++ -Wall  --std=c++11 -O3 -o BA ${ADD_FILES} BA_Framework.cpp ${ADD_LIBRARIES}

clean: 
	rm -f BA
